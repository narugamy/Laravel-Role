<?php
	
	Route::get('/', function () {
		return view('welcome');
	});
	Route::namespace('Admin')->prefix('admin')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:admin')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('admin.logout');
			});
		});
		Route::middleware('auth:admin')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('admin.index');
			});
			Route::namespace('User')->prefix('user')->group(function () {
				Route::get('/', 'UserController@index')->name('admin.user.index');
				Route::get('create', 'UserController@create')->name('admin.user.create');
				Route::post('create', 'UserController@store');
				Route::get('{user}', 'UserController@show')->name('admin.user.update');
				Route::put('{user}', 'UserController@update');
				Route::get('{user}/delete', 'UserController@delete')->name('admin.user.delete');
				Route::get('{user}/destroy', 'UserController@destroy')->name('admin.user.destroy');
				//Permissions
				Route::get('{user}/permission', 'UserController@show_permission')->name('admin.user.permission');
				Route::post('{user}/permission', 'UserController@store_permission');
			});
			Route::namespace('Role')->prefix('role')->group(function () {
				Route::get('/', 'RoleController@index')->name('admin.role.index');
				Route::get('create', 'RoleController@create')->name('admin.role.create');
				Route::post('create', 'RoleController@store');
				Route::get('{role}', 'RoleController@show')->name('admin.role.update');
				Route::put('{role}', 'RoleController@update');
				Route::get('{role}/delete', 'RoleController@delete')->name('admin.role.delete');
				Route::get('{role}/destroy', 'RoleController@destroy')->name('admin.role.destroy');
				//Permissions
				Route::get('{role}/permission', 'RoleController@show_permission')->name('admin.role.permission');
				Route::post('{role}/permission', 'RoleController@store_permission');
			});
			Route::namespace('Permission')->prefix('permission')->group(function () {
				Route::get('/', 'PermissionController@index')->name('admin.permission.index');
				Route::get('create', 'PermissionController@create')->name('admin.permission.create');
				Route::post('create', 'PermissionController@store');
				Route::get('{permission}', 'PermissionController@show')->name('admin.permission.update');
				Route::put('{permission}', 'PermissionController@update');
				Route::get('{permission}/delete', 'PermissionController@delete')->name('admin.permission.delete');
				Route::get('{permission}/destroy', 'PermissionController@destroy')->name('admin.permission.destroy');
			});
		});
	});
	Route::namespace('Developer')->prefix('programador')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('developer.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:developer')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('developer.logout');
			});
		});
		Route::middleware('auth:developer')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('developer.index');
			});
			Route::namespace('User')->prefix('user')->group(function () {
				Route::get('/', 'UserController@index')->name('developer.user.index');
				Route::get('create', 'UserController@create')->name('developer.user.create');
				Route::post('create', 'UserController@store');
				Route::get('{user}', 'UserController@show')->name('developer.user.update');
				Route::put('{user}', 'UserController@update');
				Route::get('{user}/delete', 'UserController@delete')->name('developer.user.delete');
				Route::get('{user}/destroy', 'UserController@destroy')->name('developer.user.destroy');
				//Permissions
				Route::get('{user}/permission', 'UserController@show_permission')->name('developer.user.permission');
				Route::post('{user}/permission', 'UserController@store_permission');
			});
			Route::namespace('Role')->prefix('role')->group(function () {
				Route::get('/', 'RoleController@index')->name('developer.role.index');
				Route::get('create', 'RoleController@create')->name('developer.role.create');
				Route::post('create', 'RoleController@store');
				Route::get('{role}', 'RoleController@show')->name('developer.role.update');
				Route::put('{role}', 'RoleController@update');
				Route::get('{role}/delete', 'RoleController@delete')->name('developer.role.delete');
				Route::get('{role}/destroy', 'RoleController@destroy')->name('developer.role.destroy');
				//Permissions
				Route::get('{role}/permission', 'RoleController@show_permission')->name('developer.role.permission');
				Route::post('{role}/permission', 'RoleController@store_permission');
			});
			Route::namespace('Permission')->prefix('permission')->group(function () {
				Route::get('/', 'PermissionController@index')->name('developer.permission.index');
				Route::get('create', 'PermissionController@create')->name('developer.permission.create');
				Route::post('create', 'PermissionController@store');
				Route::get('{permission}', 'PermissionController@show')->name('developer.permission.update');
				Route::put('{permission}', 'PermissionController@update');
				Route::get('{permission}/delete', 'PermissionController@delete')->name('developer.permission.delete');
				Route::get('{permission}/destroy', 'PermissionController@destroy')->name('developer.permission.destroy');
			});
		});
	});