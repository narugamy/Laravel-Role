<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['developer.user.permission', $user->username], 'class' => 'form-horizontal form-file']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<div class="md-checkbox-grid">
									@foreach($permissions as $permission)
										<div class="md-checkbox">
											<input type="checkbox" id="permission_{{$permission->id}}" class="md-check" name="permission[]" value="{{(int)$permission->id}}"
												{{ ($user->can($permission->slug))? "checked":'' }}>
											<label for="permission_{{$permission->id}}">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> {{ $permission->name }} </label>
										</div>
									@endforeach
								</div>
							</div>
							@if(count($permissions) > 0):
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button class="btn green button-submit"> Crear
									<i class="fa fa-check"></i>
								</button>
							</div>
							@else
								<div class="alert alert-danger text-center">
									<strong>No existe ningún permiso</strong>
								</div>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>