<?php

	namespace App\Model;

	use Caffeinated\Shinobi\Models\Role;
	use Caffeinated\Shinobi\Traits\ShinobiTrait;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Notifications\Notifiable;
	use Illuminate\Foundation\Auth\User as Authenticatable;

	class User extends Authenticatable {

		use Notifiable, ShinobiTrait, SoftDeletes;

		protected $table = "user";

		protected $fillable = ['names', 'surnames', 'address', 'email', 'username', 'password',];

		protected $hidden = ['password', 'remember_token',];

		public function full_name() {
			return "$this->names $this->surnames";
		}

	}
