<?php

	namespace App\Policies;

	use App\Model\User;
	use Caffeinated\Shinobi\Models\Role;
	use Illuminate\Auth\Access\HandlesAuthorization;

	class RolePolicy {

		use HandlesAuthorization;

		public function index(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-role-index");
		}

		public function create(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-role-create");
		}

		public function update(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-role-update");
		}

		public function delete(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-role-delete");
		}

		public function destroy(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-role-destroy");
		}

		public function create_permission(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-role-permission");
		}

	}
