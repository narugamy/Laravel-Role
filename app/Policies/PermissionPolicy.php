<?php

	namespace App\Policies;

	use App\Model\User;
	use Illuminate\Auth\Access\HandlesAuthorization;

	class PermissionPolicy {

		use HandlesAuthorization;

		public function index(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-permission-index");
		}

		public function create(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-permission-create");
		}

		public function update(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-permission-update");
		}

		public function delete(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-permission-delete");
		}

		public function destroy(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-permission-destroy");
		}

	}
