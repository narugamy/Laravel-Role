<?php

	namespace App\Policies;

	use App\Model\User;
	use Caffeinated\Shinobi\Facades\Shinobi;
	use Illuminate\Auth\Access\HandlesAuthorization;

	class UserPolicy {

		use HandlesAuthorization;

		public function index(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-user-index");
		}

		public function create(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-user-create");
		}

		public function update(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-user-update");
		}

		public function delete(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-user-delete");
		}

		public function destroy(User $user) {
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-user-destroy");
		}

		public function create_permission(User $user){
			$role_name = $user->getRoles()[0];
			return $user->can("$role_name-user-permission");
		}

	}
