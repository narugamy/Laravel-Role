<?php

	namespace App\Http\Requests\Admin\Role;

	use Illuminate\Foundation\Http\FormRequest;

	class CreateRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'name' => 'required|min:3|max:21|unique:role',
				'description' => 'required|min:5'
			];
		}

		public function messages(){
			return [
				'name.required'=>'requerido',
				'name.unique'=>'el usuario no existe',
				'name.min'=>'min. :min caracteres',
				'name.max'=>'max. :max caracteres',
				'description.required'=>'requerido',
				'description.min'=>'min. :min caracteres'
			];
		}

	}
