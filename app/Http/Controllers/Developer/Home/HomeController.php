<?php

	namespace App\Http\Controllers\Developer\Home;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class HomeController extends Controller {

		public function index() {
			return view('developer.home._index')->with(['title' => 'Panel de Programador', 'class_header' => 'page-container-bg-solid']);
		}

	}
