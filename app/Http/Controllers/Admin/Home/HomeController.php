<?php

	namespace App\Http\Controllers\Admin\Home;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class HomeController extends Controller {

		public function index() {
			return view('admin.home._index')->with(['title' => 'Panel de Administrador', 'class_header' => 'page-container-bg-solid']);
		}

	}
