<?php

	namespace App\Http\Controllers\Admin\User;

	use App\Http\Requests\Admin\User\CreateRequest;
	use App\Http\Requests\Admin\User\UpdateRequest;
	use App\Model\User;
	use App\Policies\UserPolicy;
	use Caffeinated\Shinobi\Models\Permission;
	use Caffeinated\Shinobi\Models\Role;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;

	class UserController extends Controller {

		public function index() {
			$this->autorizate('index');
			$users = User::withTrashed()->get();
			return view('admin.user._index')->with(['title' => 'Panel de usuarios', 'class_header' => 'page-container-bg-solid', 'users' => $users]);
		}

		public function create(Request $request) {
			$this->autorizate('create');
			$roles = Role::orderBy('name')->pluck('name', 'id');
			if ($request->ajax()):
				$data = view('admin.user._Create');
			else:
				$data = view('admin.user.Create')->with(['title' => 'Registro de usuario']);
			endif;
			return $data->with(['roles' => $roles]);
		}

		public function store(CreateRequest $request) {
			$this->autorizate('create');
			try {
				DB::beginTransaction();
				$user = new User($request->all());
				$user->password = bcrypt($user->password);
				$user->save();
				$user->assignRole($request->role_id);
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function store_permission(Request $request, $username) {
			$this->autorizate('create_permission');
			$user = User::where(['username' => $username])->first();
			if($user):
				try {
					DB::beginTransaction();
					$user->syncPermissions($request->permission);
					$user->save();
					DB::commit();
					$message = "Registro exitoso";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('admin.user.permission', $user->username), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el usuario no existe";
				$array = (object) ['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $username) {
			$this->autorizate('update');
			$user = User::where(['username' => $username])->first();
			if ($user):
				$role = Role::where(['name' => $user->getRoles()[0]])->first();
				$roles = Role::orderBy('name')->pluck('name', 'id');
				if ($request->ajax()):
					$data = view('admin.user._Update');
				else:
					$data = view('admin.user.Update')->with(['title' => "Actualización del usuario: $user->full_name()"]);
				endif;
				$data->with(['user' => $user, 'role' => $role, 'roles' => $roles]);
			else:
				if ($request->ajax()):
					$data = "Usuario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el usuario no existe");
					$data = redirect(route('admin.user.index'));
				endif;
			endif;
			return $data;
		}

		public function show_permission(Request $request, $username) {
			$this->autorizate('create_permission');
			$user = User::where(['username' => $username])->first();
			if ($user):
				$role_name = $user->getRoles()[0];
				$user_permissions = array_diff($user->getPermissions(),$user->getUserPermissions());
				$permissions = Permission::where('route','LIKE',"$role_name%")->WhereNotIn('slug', $user_permissions)->get();
				if ($request->ajax()):
					$data = view('admin.user._Permission');
				else:
					$data = view('admin.user.Permission')->with(['title' => "Permisos para el usuario: $user->full_name()"]);
				endif;
				$data->with(['permissions' => $permissions, 'user' => $user]);
			else:
				if ($request->ajax()):
					$data = "Usuario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el usuario no existe");
					$data = redirect(route('admin.user.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $username) {
			$this->autorizate('update');
			$user = User::where(['username' => $username])->first();
			if ($user):
				try {
					DB::beginTransaction();
					$password = $user->password;
					$user->fill($request->all());
					$user->password = (!empty($user->password))? bcrypt($request->password):$password;
					$user->syncRoles([$request->role_id]);
					$user->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object) ['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object) ['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.update', $user->username), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el curso no existe";
				$array = (object) ['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete (Request $request, $username) {
			$this->autorizate('delete');
			if ($request->ajax()):
				$user = User::withTrashed()->where(['username' => $username])->first();
				if ($user):
					try {
						DB::beginTransaction();
						if ($user->deleted_at):
							$user->restore();
							$message = "Restauración exitosa";
						else:
							$user->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		public function destroy (Request $request, $username) {
			$this->autorizate('destroy');
			if ($request->ajax()):
				$user = User::withTrashed()->where(['username' => $username])->first();
				if ($user):
					try {
						DB::beginTransaction();
						$user->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		private function autorizate($function){
			$this->authorize($function, User::class);
		}

	}
