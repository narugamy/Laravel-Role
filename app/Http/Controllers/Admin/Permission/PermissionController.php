<?php

	namespace App\Http\Controllers\Admin\Permission;

	use App\Http\Requests\Admin\Permission\CreateRequest;
	use App\Http\Requests\Admin\Permission\UpdateRequest;
	use Caffeinated\Shinobi\Models\Permission;
	use Caffeinated\Shinobi\Models\Role;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;

	class PermissionController extends Controller {

		public function index() {
			$this->autorizate('index');
			$permissions = Permission::withTrashed()->get();
			return view('admin.permission._index')->with(['title' => 'Panel de permisos', 'class_header' => 'page-container-bg-solid', 'permissions' => $permissions]);
		}

		public function create(Request $request) {
			$this->autorizate('create');
			if ($request->ajax()):
				$data = view('admin.permission._Create');
			else:
				$data = view('admin.permission.Create')->with(['title' => 'Registro de permiso']);
			endif;
			return $data;
		}

		public function store(CreateRequest $request) {
			$this->autorizate('create');
			try {
				DB::beginTransaction();
				$permission = new Permission($request->all());
				$permission->slug = str_replace('.', '-', strtolower($permission->route));
				$permission->save();
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.permission.index')], 'status' => 200, 'route' => route('admin.permission.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.permission.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			$this->autorizate('update');
			$permission = Permission::where(['slug' => $slug])->first();
			if ($permission):
				if ($request->ajax()):
					$data = view('admin.permission._Update');
				else:
					$data = view('admin.permission.Update')->with(['title' => "Actualización del permiso: $permission->name"]);
				endif;
				$data->with(['permission' => $permission]);
			else:
				if ($request->ajax()):
					$data = "Rol no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el rol no existe");
					$data = redirect(route('admin.rol.index'));
				endif;
			endif;
			return $data;
		}

		public function update(UpdateRequest $request, $slug) {
			$this->autorizate('update');
			$permission = Permission::where(['slug' => $slug])->first();
			if ($permission):
				try {
					DB::beginTransaction();
					$permission->fill($request->all())->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object) ['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.permission.index')], 'status' => 200, 'route' => route('admin.permission.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object) ['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.permission.update', $permission->name), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el curso no existe";
				$array = (object) ['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.permission.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete (Request $request, $slug) {
			$this->autorizate('delete');
			if ($request->ajax()):
				$permission = Permission::withTrashed()->where(['slug' => $slug])->first();
				if ($permission):
					try {
						DB::beginTransaction();
						if ($permission->deleted_at):
							$permission->restore();
							$message = "Restauración exitosa";
						else:
							$permission->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.permission.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el permiso no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.permission.index'));
			endif;
			return $data;
		}

		public function destroy (Request $request, $slug) {
			$this->autorizate('destroy');
			if ($request->ajax()):
				$permission = Permission::withTrashed()->where(['slug' => $slug])->first();
				if ($permission):
					try {
						DB::beginTransaction();
						$permission->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.permission.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el permiso no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.permission.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		private function autorizate($function){
			$this->authorize($function, Permission::class);
		}

	}
