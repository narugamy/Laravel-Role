<?php

	namespace Caffeinated\Shinobi\Models;

	use Config;
	use Illuminate\Database\Eloquent\Model;
	use Caffeinated\Shinobi\Traits\PermissionTrait;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Role extends Model {

		use PermissionTrait {
			flushPermissionCache as parentFlushPermissionCache;
		}

		use SoftDeletes;

		protected $fillable = ['name', 'slug', 'description', 'special'];

		protected $table = 'role';

		public static function getShinobiTag() {
			return 'shinobi.roles';
		}

		public function users() {
			return $this->belongsToMany(config('auth.model') ?: config('auth.providers.users.model'))->withTimestamps();
		}

		protected function getFreshPermissions() {
			return $this->permissions->pluck('slug')->all();
		}

		public function getRouteKeyName() {
			return 'slug';
		}

		public function flushPermissionCache() {
			$userClass = config('auth.model') ?: config('auth.providers.users.model');
			$usersTag = call_user_func([$userClass, 'getShinobiTag']);
			static::parentFlushPermissionCache([static::getShinobiTag(), $usersTag,]);
		}

		public function can($permission) {
			if ($this->special === 'no-access'):
				return false;
			endif;
			if ($this->special === 'all-access'):
				return true;
			endif;
			return $this->hasAllPermissions($permission, $this->getPermissions());
		}

		public function canAtLeast(array $permission = []) {
			if ($this->special === 'no-access'):
				return false;
			endif;
			if ($this->special === 'all-access'):
				return true;
			endif;
			return $this->hasAnyPermission($permission, $this->getPermissions());
		}

	}
