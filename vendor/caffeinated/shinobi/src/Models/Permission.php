<?php

	namespace Caffeinated\Shinobi\Models;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Permission extends Model {

		use SoftDeletes;

		protected $fillable = ['name', 'route', 'slug', 'description'];

		protected $table = 'permission';

		public function roles() {
			return $this->belongsToMany('\Caffeinated\Shinobi\Models\Role')->withTimestamps();
		}

		public function assignRole($roleId = null) {
			$roles = $this->roles;
			if (!$roles->contains($roleId)):
				return $this->roles()->attach($roleId);
			endif;
			return false;
		}

		public function revokeRole($roleId = '') {
			return $this->roles()->detach($roleId);
		}

		public function syncRoles(array $roleIds = []) {
			return $this->roles()->sync($roleIds);
		}

		public function revokeAllRoles() {
			return $this->roles()->detach();
		}

	}
